package com.example.evan.smartvpn

import android.text.TextUtils
import android.util.Log
import org.junit.Test
import java.io.File

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {


    fun isNumeric(str: String): Boolean {



        var str1 = str.trim()

        var i = str1.length
        while (--i >= 0) {

            if (!Character.isDigit(str1[i])) {

                return false

            }

        }

        return true

    }

    @Test
    fun addition_isCorrect() {

        var processList = ArrayList<Int>()
        for (process in File("/proc").listFiles { _, name -> isNumeric(name) }) {



            var intext = ""
            try {
                intext = File(process, "cmdline").readText()
            }
            catch (e : Throwable) {
                e.stackTrace
            }



            if ((!intext.isEmpty()) && (intext.toLowerCase().contains(Executable.CHIMNEY)
                        || intext.toLowerCase().contains(Executable.NETSTACK))){

                var pid = process.name


                processList.add(pid.toInt())

            }
        }


    }
}
