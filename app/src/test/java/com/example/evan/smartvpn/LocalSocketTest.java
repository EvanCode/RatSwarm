package com.example.evan.smartvpn;

import android.net.Credentials;
import android.net.LocalServerSocket;
import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import org.junit.Test;

import java.io.FileDescriptor;

import static junit.framework.TestCase.assertEquals;

public class LocalSocketTest {

    @Test
    public void testBasic() throws Exception {
        LocalServerSocket ss;
        LocalSocket ls;
        LocalSocket ls1;

        ss = new LocalServerSocket("android.net.LocalSocketTest");

        ls = new LocalSocket();

        ls.connect(new LocalSocketAddress("android.net.LocalSocketTest"));

        ls1 = ss.accept();

        // Test trivial read and write
        ls.getOutputStream().write(42);

        assertEquals(42, ls1.getInputStream().read());



        // Test sending and receiving file descriptors
        ls.setFileDescriptorsForSend(
                new FileDescriptor[]{FileDescriptor.in});

        ls.getOutputStream().write(42);

        assertEquals(42, ls1.getInputStream().read());

        FileDescriptor[] out = ls1.getAncillaryFileDescriptors();

        assertEquals(1, out.length);


        ls1.close();
    }
}
