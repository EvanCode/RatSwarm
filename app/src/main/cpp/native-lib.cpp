#include <jni.h>
#include <string>
#include <android/log.h>

#include <sys/system_properties.h>

#include <algorithm>
#include <cerrno>
#include <csignal>
#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <arpa/inet.h>
#include <unistd.h>
#include <sys/un.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include "libancillary/ancillary.h"

#define LOG_TAG "native-lib"

static int sdk_version;
static jclass ProcessImpl;
static jfieldID ProcessImpl_pid, ProcessImpl_exitValue, ProcessImpl_exitValueMutex;

#define LOGI(...) do { __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__); } while(0)
#define LOGW(...) do { __android_log_print(ANDROID_LOG_WARN, LOG_TAG, __VA_ARGS__); } while(0)
#define LOGE(...) do { __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__); } while(0)
#define THROW(env, clazz, msg) do { env->ThrowNew(env->FindClass(clazz), msg); } while (0)

extern "C" JNIEXPORT jstring JNICALL
Java_com_example_evan_smartvpn_HowTo_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}


extern "C"
JNIEXPORT jint JNICALL
Java_com_example_evan_smartvpn_HowTo_sigkill(JNIEnv *env, jclass type, jint pid) {

   return kill(pid, SIGKILL) == -1 && errno != ESRCH ? errno : 0;

}

extern "C"
JNIEXPORT jint JNICALL
Java_com_example_evan_smartvpn_HowTo_sigterm(JNIEnv *env, jclass type, jobject process) {

    if (!env->IsInstanceOf(process, ProcessImpl)) {
        THROW(env, "java/lang/ClassCastException",
              "Unsupported process object. Only java.lang.ProcessManager$ProcessImpl is accepted.");
        return -1;
    }
    jint pid = env->GetIntField(process, ProcessImpl_pid);
    // Suppress "No such process" errors. We just want the process killed. It's fine if it's already killed.
    return kill(pid, SIGTERM) == -1 && errno != ESRCH ? errno : 0;

}

extern "C"
JNIEXPORT jobject JNICALL
Java_com_example_evan_smartvpn_HowTo_getExitValue(JNIEnv *env, jclass type, jobject process) {

    if (!env->IsInstanceOf(process, ProcessImpl)) {
        THROW(env, "java/lang/ClassCastException",
              "Unsupported process object. Only java.lang.ProcessManager$ProcessImpl is accepted.");
        return NULL;
    }
    return env->GetObjectField(process, ProcessImpl_exitValue);

}

extern "C"
JNIEXPORT jobject JNICALL
Java_com_example_evan_smartvpn_HowTo_getExitValueMutex(JNIEnv *env, jclass type, jobject process) {

    if (!env->IsInstanceOf(process, ProcessImpl)) {
        THROW(env, "java/lang/ClassCastException",
              "Unsupported process object. Only java.lang.ProcessManager$ProcessImpl is accepted.");
        return NULL;
    }
    return env->GetObjectField(process, ProcessImpl_exitValueMutex);

}

extern "C"
JNIEXPORT jint JNICALL
Java_com_example_evan_smartvpn_HowTo_sendFd(JNIEnv *env, jclass type, jint tun, jstring path_) {
    int fd;
    struct sockaddr_un addr;
    const char *path = env->GetStringUTFChars(path_, 0);

    if ( (fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
        LOGE("socket() failed: %s (socket fd = %d)\n", strerror(errno), fd);
        env->ReleaseStringUTFChars(path_, path);
        return (jint)-1;
    }

    LOGE("unix socket address : %s ", path);

    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, path, sizeof(addr.sun_path)-1);

    if (connect(fd, (struct sockaddr*)&addr, sizeof(addr)) == -1) {
        LOGE("connect() failed: %s (fd = %d)\n", strerror(errno), fd);
        close(fd);
        env->ReleaseStringUTFChars(path_, path);
        return (jint)-1;
    }

    if (ancil_send_fd(fd, tun)) {
        LOGE("ancil_send_fd: %s", strerror(errno));
        close(fd);
        env->ReleaseStringUTFChars(path_, path);
        return (jint)-1;
    }

    close(fd);

    env->ReleaseStringUTFChars(path_, path);

    return 0;
}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_evan_smartvpn_HowTo_close(JNIEnv *env, jclass type, jint fd) {

    close(fd);

}


typedef union {
    JNIEnv* env;
    void* venv;
} UnionJNIEnvToVoid;

jint JNI_OnLoad(JavaVM* vm, void* reserved) {
    UnionJNIEnvToVoid uenv;
    uenv.venv = NULL;
    jint result = -1;
    JNIEnv* env = NULL;

    if (vm->GetEnv(&uenv.venv, JNI_VERSION_1_6) != JNI_OK) {
        THROW(env, "java/lang/RuntimeException", "GetEnv failed");
        goto bail;
    }
    env = uenv.env;

    char version[PROP_VALUE_MAX + 1];
    __system_property_get("ro.build.version.sdk", version);
    sdk_version = atoi(version);

#define FIND_CLASS(out, name)                                                           \
    if (!(out = env->FindClass(name))) {                                                \
        THROW(env, "java/lang/RuntimeException", name " not found");                    \
        goto bail;                                                                      \
    }                                                                                   \
    out = reinterpret_cast<jclass>(env->NewGlobalRef(reinterpret_cast<jobject>(out)))
#define GET_FIELD(out, clazz, name, sig)                                                                    \
    if (!(out = env->GetFieldID(clazz, name, sig))) {                                                       \
        THROW(env, "java/lang/RuntimeException", "Field " #clazz "." name " with type " sig " not found");  \
        goto bail;                                                                                          \
    }

    if (sdk_version < 24) {
        FIND_CLASS(ProcessImpl, "java/lang/ProcessManager$ProcessImpl");
        GET_FIELD(ProcessImpl_pid, ProcessImpl, "pid", "I")
        GET_FIELD(ProcessImpl_exitValue, ProcessImpl, "exitValue", "Ljava/lang/Integer;")
        GET_FIELD(ProcessImpl_exitValueMutex, ProcessImpl, "exitValueMutex", "Ljava/lang/Object;")
    }

    result = JNI_VERSION_1_6;

    bail:
    return result;
}

extern "C"
JNIEXPORT jint JNICALL
Java_com_example_evan_smartvpn_HowTo_sendCommand(JNIEnv *env, jclass type, jint command, jstring path_) {
    int fd;
    ssize_t  n = 0;
    struct sockaddr_un addr;
    const char *path = env->GetStringUTFChars(path_, 0);

    if ( (fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
        LOGE("socket() failed: %s (socket fd = %d)\n", strerror(errno), fd);
        env->ReleaseStringUTFChars(path_, path);
        return (jint)-1;
    }

    LOGE("unix socket address : %s ", path);

    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, path, sizeof(addr.sun_path)-1);

    if (connect(fd, (struct sockaddr*)&addr, sizeof(addr)) == -1) {
        LOGE("connect() failed: %s (fd = %d)\n", strerror(errno), fd);
        close(fd);
        env->ReleaseStringUTFChars(path_, path);
        return (jint)-1;
    }

    n  = send(fd,&command, sizeof(command), 0);
    LOGE("send bytes %d", n);

    close(fd);

    env->ReleaseStringUTFChars(path_, path);

    return 0;
}