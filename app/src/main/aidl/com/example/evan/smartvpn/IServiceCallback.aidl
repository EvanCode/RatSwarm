// IServiceCallback.aidl
package com.example.evan.smartvpn;

// Declare any non-default types here with import statements

interface IServiceCallback {

   void updateTraffic(long up, long down);

   void updateServiceState(int state, String errorMsg);
}
