// IService.aidl
package com.example.evan.smartvpn;

import com.example.evan.smartvpn.IServiceCallback;
import com.example.evan.smartvpn.ServiceExtra;

interface IService {

  int queryServiceState();

  int registerCallback(IServiceCallback call);

  int unregisterCallback(IServiceCallback call);

  int startService(in ServiceExtra data);


}
