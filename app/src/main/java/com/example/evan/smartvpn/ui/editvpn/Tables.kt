package com.example.evan.smartvpn.ui.editvpn


object VPNTables {
    val TABLE_NAME = "VPNT"
    val ID = "_id"
    val NAME = "name"
    val SERVER = "server"
    val REMOTEPORT = "remoteport"
    val PASSWORD ="password"
    val DNS="remoteDNS"

}