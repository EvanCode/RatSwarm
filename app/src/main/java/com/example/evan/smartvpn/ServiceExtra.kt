package com.example.evan.smartvpn

import android.os.Parcel
import android.os.Parcelable

class ServiceExtra() : Parcelable {

    var server: String = ""
    var port : Int = 0
    var password :String = ""
    var dns :String = ""



    constructor(parcel: Parcel) : this() {
        this.readFromParcel(parcel)
    }

    override fun writeToParcel(out: Parcel, flags: Int) {
        out.writeString(server)
        out.writeInt(port)
        out.writeString(password)
        out.writeString(dns)
    }

    override fun describeContents(): Int {
        return 0
    }

    private fun readFromParcel(inParcel: Parcel) {
        this.server = inParcel.readString()
        this.port = inParcel.readInt()
        this.password = inParcel.readString()
        this.dns = inParcel.readString()
    }

    companion object CREATOR : Parcelable.Creator<ServiceExtra> {
        override fun createFromParcel(parcel: Parcel): ServiceExtra {
            return ServiceExtra(parcel)
        }

        override fun newArray(size: Int): Array<ServiceExtra?> {
            return arrayOfNulls(size)
        }
    }
}