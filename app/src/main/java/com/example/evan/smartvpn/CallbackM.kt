package com.example.evan.smartvpn

import android.util.Log

class CallbackM {


    companion object {
        var instance: CallbackM = CallbackM()
            private set
    }

    var callback : IServiceCallback? = null

    fun register(call : IServiceCallback?){

        Log.d("XXXXXXMMMM", "register IServiceCallback   " + (call == null))
        this.callback = call
    }

    fun unregister(call: IServiceCallback?) {
        Log.d("XXXXXXMMMM", "unregister IServiceCallback")
        this.callback = null
    }


    fun updateTraffic(up: Long, down: Long){
        Log.d("XXXXXXMMMM", "update traffic!!")
        if (this.callback != null) {
            Log.d("XXXXXXMMMM", "done traffic!!")
            this.callback!!.updateTraffic(up, down)
        }
    }

    fun updateServiceState(state: Int, errorMsg: String){
        Log.d("XXXXXXMMMM", "updateServiceState!!")
        if (this.callback != null) {
            this.callback!!.updateServiceState(state, errorMsg)
        }
    }
}