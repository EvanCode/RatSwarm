package com.example.evan.smartvpn

interface UIUpdateInterface  {

    fun register(v : UIUpdateInterface)

    fun unregister(v : UIUpdateInterface)

    fun nofityUI()

}