package com.example.evan.smartvpn

class ServiceM {

    companion object {
        var instance: ServiceM = ServiceM()
            private set
    }

    var ms : IService? = null

    fun plugin(s: IService){

        ms = s

    }

    fun unPlugin(s: IService){

        ms = null

    }

    fun queryServiceState(): Int {

        if (ms != null) {
            return ms!!.queryServiceState()
        }

        return -1
    }

    fun registerCallback( call : IServiceCallback): Int {

        return 0

    }

    fun unregisterCallback( call : IServiceCallback): Int {

        return 0

    }

    fun startService( data : ServiceExtra): Int {

        return 0

    }
}