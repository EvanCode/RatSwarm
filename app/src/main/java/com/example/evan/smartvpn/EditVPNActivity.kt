package com.example.evan.smartvpn

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.example.evan.smartvpn.ui.editvpn.VPNOpDao
import com.example.evan.smartvpn.ui.editvpn.VPNProfile
import kotlinx.android.synthetic.main.activity_edit_vpn.*
import kotlinx.android.synthetic.main.content_edit_vpn.*
import java.util.regex.Pattern

class EditVPNActivity : AppCompatActivity() {

    var isNew : Boolean = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_vpn)
        setSupportActionBar(toolbar)
        isNew = this.intent!!.getBooleanExtra("new", true)

        var vpn : VPNProfile? = null

        if (isNew) {
            remotedns.setText("1.1.1.1")
        }
        else {
            var position = this.intent.getIntExtra("data", -1)
            var dao = VPNOpDao()
            var list = dao.query()
            if (position != -1 && list.count() > position) {
                vpn = list[position]

                this.vpnname.setText(vpn.name)
                this.servername.setText(vpn.server)
                this.remoteport.setText(vpn.remoteport.toString())
                this.passwd.setText(vpn.password)
                this.remotedns.setText(vpn.remoteDNS)
            }
        }

        fab.setOnClickListener { view ->

            if (!checkBlankValue()){
                Snackbar.make(view, "please fill every item", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
                return@setOnClickListener
            }

            if (!checkPortValue()){
                Snackbar.make(view, "the port value is 1-65535.", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
                return@setOnClickListener
            }

            if (!checkIPAddress()) {
                Snackbar.make(view, "please check ip address or URL", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
                return@setOnClickListener
            }

            saveToDB(vpn)

            NotifyCenter.instance.nofityUI()

            Toast.makeText(this.applicationContext, "save success!", Toast.LENGTH_LONG).show()

            finish()

        }
    }


    fun saveToDB(v: VPNProfile?) {

        var vpn = VPNProfile()
        vpn.name = this.vpnname.text.toString().trim()
        vpn.server = this.servername.text.toString().trim()
        vpn.remoteport = this.remoteport.text.toString().toInt()
        vpn.remoteDNS = this.remotedns.text.toString().trim()
        vpn.password = this.passwd.text.toString().trim()


        var dao = VPNOpDao()
        if (this.isNew) {
            dao.addVPN(vpn)
        }
        else {
            vpn._id = v!!._id
            dao.updateVPN(vpn)
        }
    }

    fun checkIPAddress() :Boolean {

       var patten =  Pattern.compile(
            "((25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(25[0-5]|2[0-4]"
                    + "[0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]"
                    + "[0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}"
                    + "|[1-9][0-9]|[0-9]))")

        var name = this.servername.text.toString()
        var dns = this.remotedns.text.toString()

        if ((name.startsWith("http") ||
                    patten.matcher(name).matches()) && (dns.startsWith("http")
                    || patten.matcher(dns).matches() )){
            return true
        }

        return false
    }

    fun checkPortValue() : Boolean {
        var port = this.remoteport.text.toString().toInt()
        if (port < 1) {
            return false
        }

        return true
    }

    fun checkBlankValue() : Boolean  {
        var vpn = this.servername.text
        var port = this.remoteport.text
        var pass = this.passwd.text
        var dns = this.remotedns.text

        if (vpn.isBlank()|| this.vpnname.text.isBlank()|| port.isBlank()|| pass.isBlank() || dns.isBlank()){
            return false
        }

        return true
    }

}
