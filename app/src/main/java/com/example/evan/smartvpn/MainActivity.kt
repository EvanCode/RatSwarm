package com.example.evan.smartvpn


import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.net.VpnService
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Message
import android.support.design.widget.Snackbar
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.*
import android.widget.*
import com.example.evan.smartvpn.ui.editvpn.VPNOpDao
import com.example.evan.smartvpn.ui.editvpn.VPNProfile
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import org.json.JSONObject
import java.io.File
import kotlin.concurrent.thread


class MainActivity : AppCompatActivity(), UIUpdateInterface {


    override fun register(v: UIUpdateInterface) {
        NotifyCenter.instance.register(v)

    }

    override fun unregister(v: UIUpdateInterface) {
        NotifyCenter.instance.unregister(v)

    }

    override fun nofityUI() {
        var a = this.vpnprofiles.adapter as VpnAdapter
        a.update()
        a.notifyDataSetChanged()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        unregister(this)
    }


    var serviceState: Int = -1


    fun genserviceConfigFile(vpn: VPNProfile) {
        var file = File(this.applicationContext.filesDir, "sxxx.json")
        file.delete()

        var json = JSONObject()
        json.put("server", vpn.server)
        json.put("server_port", vpn.remoteport)
        json.put("local_port", 1080)
        json.put("local_address", "127.0.0.1")
        json.put("password", vpn.password.trim())
        json.put("dns", vpn.remoteDNS)


        file.writeText(json.toString())
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        setTitle(R.string.title_name)
        register(this)

        addvpnbtn.setOnClickListener {
            val mIntent = Intent(this@MainActivity, EditVPNActivity::class.java)
            mIntent.putExtra("new", true)
            startActivity(mIntent)
        }



        fab.setOnClickListener { view ->
            var adp = vpnprofiles.adapter as VpnAdapter
            if (adp.count <= 0) {
                Snackbar.make(view, R.string.emptyprompt, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
            } else {
                if (CustomApp.instance.selectedItemId >= 0 && CustomApp.instance.selectedItemId  < vpnprofiles.count) {

                    var vpn = vpnprofiles.adapter.getItem(CustomApp.instance.selectedItemId ) as VPNProfile
                    genserviceConfigFile(vpn)

                    if (1 == serviceState) {

                        changeButton(3)
                        stopService()

                        serviceState = 0

                    } else {
                        serviceState = 0
                        changeButton(3)
                        //start
                        startService()
                    }

                } else {
                    Snackbar.make(view, R.string.selectitem, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
                }
            }
        }

        var vpnadapter = VpnAdapter(this.baseContext)
        vpnprofiles.adapter = vpnadapter

        vpnprofiles.setOnItemClickListener { adapterView: AdapterView<*>, view1: View, i: Int, l: Long ->

            var profile = vpnprofiles.adapter as VpnAdapter
            var vpn = profile.getItem(i) as VPNProfile

            Toast.makeText(this@MainActivity.baseContext, vpn.toString(), Toast.LENGTH_LONG).show()
            CustomApp.instance.selectedItemId  = i
        }


        vpnprofiles.choiceMode = AbsListView.CHOICE_MODE_SINGLE

        Toast.makeText(this.applicationContext, "I'll back!!", Toast.LENGTH_LONG).show()

        toolbar.setOnMenuItemClickListener { it: MenuItem? ->

            if (it!!.itemId == R.id.action_about) {

                Toast.makeText(this.applicationContext, "zwh2698@gmail.com", Toast.LENGTH_LONG).show()
            }

            if (it.itemId == R.id.action_settings) {
                Toast.makeText(this.applicationContext, "zwh2698@gmail.com", Toast.LENGTH_LONG).show()
            }

            true
        }

       var state =  ServiceM.instance.queryServiceState()
        changeButton(state)

    }

    fun changeButton(state: Int) {

        if (0 == state) {
            fab.foreground = ResourcesCompat.getDrawable(this.resources, R.drawable.button_bg, null)
            fab.background = ResourcesCompat.getDrawable(this.resources, R.drawable.button_bg, null)
            vpnprofiles.isEnabled = true
        }

        if (1 == state) {
            fab.foreground = ResourcesCompat.getDrawable(this.resources, R.drawable.button_bg_2, null)
            fab.background = ResourcesCompat.getDrawable(this.resources, R.drawable.button_bg_2, null)
            vpnprofiles.isEnabled = true

        }

        if (2 == state) {
            fab.foreground = ResourcesCompat.getDrawable(this.resources, R.drawable.button_bg_1, null)
            fab.background = ResourcesCompat.getDrawable(this.resources, R.drawable.button_bg_1, null)
            vpnprofiles.isEnabled = false
        }

        if (3 == state) {
            fab.foreground = ResourcesCompat.getDrawable(this.resources, R.drawable.button_bg_3, null)
            fab.background = ResourcesCompat.getDrawable(this.resources, R.drawable.button_bg_3, null)
            vpnprofiles.isEnabled = true
        }
    }


    fun startService() {

        val intent = VpnService.prepare(this)
        if (intent != null) {
            startActivityForResult(intent, 0)
            var adapter = this.vpnprofiles.adapter as VpnAdapter
            bindService(intent, adapter.mSecondaryConnection, 0)

        } else {
            onActivityResult(0, Activity.RESULT_OK, null)
        }

    }

    fun stopService() {
        var intent = Intent(this, MyNetWorkService::class.java)
        intent.putExtra("stop_flag", true)
        startService(intent)
        //ServiceM.instance.startService(ServiceExtra())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {

            thread {
                var intent = Intent(this, MyNetWorkService::class.java)
                startService(intent)

                var adapter = this.vpnprofiles.adapter as VpnAdapter
                bindService(intent, adapter.mSecondaryConnection, 0)
            }.start()

        } else {
            Snackbar.make(this.vpnprofiles, "permission denied!!", Snackbar.LENGTH_LONG).show()

        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    inner class VpnAdapter(var ctx: Context) : BaseAdapter() {

        private val mInflator: LayoutInflater
        private val viewList: HashMap<Int, ViewHolder>
        private var listData: List<VPNProfile>
        private var up: Long = -1
        private var down: Long = -1

        init {
            this.mInflator = LayoutInflater.from(ctx)
            this.viewList = HashMap<Int, ViewHolder>()
            var dao = VPNOpDao()
            this.listData = dao.query()

        }

        fun update() {
            var dao = VPNOpDao()
            this.listData = dao.query()
        }


        override fun getItem(position: Int): Any {
            return this.listData[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {

            return this.listData.size
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

            var viewHolder = ViewHolder()
            if (convertView == null) {
                var view = this.mInflator.inflate(R.layout.vpnitem, parent, false)
                viewHolder.dataFlow = view.findViewById(R.id.dataflow)
                viewHolder.all = view
                viewHolder.vpnName = view.findViewById(R.id.vpnName)
                viewHolder.deletebtn = view.findViewById(R.id.vpndeletevpn)
                viewHolder.editbtn = view.findViewById(R.id.vpneditbtn)
                viewHolder.position = position
                viewHolder.editbtn!!.tag = viewHolder
                viewHolder.deletebtn!!.tag = viewHolder
                view.tag = viewHolder
                viewHolder.adapter = this
                this.viewList.put(position, viewHolder)

                viewHolder.editbtn!!.setOnClickListener(object : View.OnClickListener {
                    override fun onClick(v: View?) {
                        var holder = v!!.tag as ViewHolder
                        if (holder.position >= 0 && holder.position < holder.adapter!!.count) {
                            val mIntent = Intent(holder.all!!.context, EditVPNActivity::class.java)
                            mIntent.putExtra("new", false)
                            mIntent.putExtra("data", holder.position)
                            holder.all!!.context.startActivity(mIntent)
                        }
                    }
                })

                viewHolder.deletebtn!!.setOnClickListener(object : View.OnClickListener {
                    override fun onClick(v: View?) {
                        var holder = v!!.tag as ViewHolder
                        if (holder.position >= 0 && holder.position < holder.adapter!!.count) {
                            var vpn = holder.adapter!!.getItem(holder.position) as VPNProfile
                            var dao = VPNOpDao()
                            dao.deleteOne(vpn._id.toInt())
                            holder.adapter!!.update()
                            holder.adapter!!.notifyDataSetChanged()
                            Toast.makeText(
                                parent!!.context,
                                "[" + vpn.name + "]" + " was removed successfully.",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
                })

            } else {
                viewHolder = convertView.tag as ViewHolder

            }

            viewHolder.vpnName!!.text = this.listData[position].name
            viewHolder.dataFlow!!.text = " ↑ 0 Kb/s ↓ 0 KB/s "

            if (CustomApp.instance.selectedItemId == position) {
                var out = "↑ " + FormatUtils().Format(this.up) +
                        "  ↓ " + FormatUtils().Format(this.down)

                viewHolder.dataFlow!!.text = out
            }


            return viewHolder.all!!

        }


        var UIHandler = object : Handler() {

            override fun handleMessage(msg: Message) {
                //super.handleMessage(msg)

                if (msg.what == 1) {
                    Log.d("DDDDDDzzDDDDD", up.toString() + down.toString())

                    this@VpnAdapter.up = msg.arg1.toLong()
                    this@VpnAdapter.down = msg.arg2.toLong()
                    this@VpnAdapter.notifyDataSetChanged()

                }

                if (msg.what == 2) {

                    var text = msg.obj as String
                    if (!text.isEmpty()) {
                        Toast.makeText(this@MainActivity.baseContext, text, Toast.LENGTH_LONG)
                    }

                    serviceState = msg.arg1
                    if (msg.arg1 == 1) {

                        this@MainActivity.changeButton(1)

                    }

                    if (msg.arg1 == 0) {

                        this@MainActivity.changeButton(0)

                    }
                    if (msg.arg1 == 2) {
                        this@MainActivity.changeButton(2)
                    }

                }
            }

        }


        val serviceCallback = object : IServiceCallback.Stub() {
            override fun updateTraffic(up: Long, down: Long) {

                Log.d("XXXXXXMMMM", "updateTraffic MAINVIEW" + up.toString() + "-" + down.toString())

                var msg = Message()
                msg.what = 1
                msg.arg1 = up.toInt()
                msg.arg2 = down.toInt()
                UIHandler.sendMessage(msg)

            }

            override fun updateServiceState(state: Int, errorMsg: String?) {
                Log.d("XXXXXXMMMM", "updateServiceState MAINVIEW")

                var msg = Message()
                msg.what = 2
                msg.arg1 = state
                msg.obj = errorMsg
                UIHandler.sendMessage(msg)

            }

        }


        val mSecondaryConnection = object : ServiceConnection {

            var mService: IService? = null

            override fun onServiceDisconnected(name: ComponentName?) {
                Log.d("XXXXXXMMMM", "onServiceDisconnected")
                if (mService != null) {
                    //         mService!!.unregisterCallback(serviceCallback)
                    //         ServiceM.instance.unPlugin(mService!!)
                    mService = null
                }

            }

            override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
                Log.d("XXXXXXMMMM", "onServiceConnected")

                mService = IService.Stub.asInterface(service)
                ServiceM.instance.plugin(mService!!)

                Log.d("XXXXXXMMMM", "mService" + (mService == null).toString())
                if (mService != null) {

                    Thread {
                        if (mService != null) {
                            mService!!.registerCallback(serviceCallback)
                        }
                    }.start()
                }

            }
        }

    }

    class ViewHolder {
        var vpnName: TextView?
        var dataFlow: TextView?
        var deletebtn: ImageButton?
        var editbtn: ImageButton?
        var all: View?
        var position: Int
        var adapter: VpnAdapter?

        constructor() {
            this.vpnName = null
            this.dataFlow = null
            this.deletebtn = null
            this.editbtn = null
            this.all = null
            this.position = -1
            this.adapter = null
        }

    }


}
