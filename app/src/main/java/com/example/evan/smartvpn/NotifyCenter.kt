package com.example.evan.smartvpn




class NotifyCenter : UIUpdateInterface {

    var updater = ArrayList<UIUpdateInterface>()


    override fun register(v: UIUpdateInterface) {
        updater.add(v)

    }

    override fun unregister(v: UIUpdateInterface) {
        updater.remove(v)

    }

    override fun nofityUI() {
        for (v in updater) {
            v.nofityUI()
        }
    }

    companion object {

        val instance by lazy { NotifyCenter() }
    }

}