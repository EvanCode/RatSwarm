package com.example.evan.smartvpn

import android.app.IntentService
import android.app.Service
import android.content.Intent
import android.net.LocalServerSocket
import android.net.LocalSocket
import android.net.LocalSocketAddress
import android.net.VpnService
import android.os.IBinder
import android.os.ParcelFileDescriptor
import android.util.Log
import chimney.Chimney
import chimney.IDataFlow
import chimney.ISocket
import org.jetbrains.anko.progressDialog
import org.json.JSONObject
import java.io.File
import java.io.FileDescriptor
import java.io.IOException
import java.lang.reflect.Method

// TODO: Rename actions, choose action names that describe tasks that this
// IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
private const val ACTION_FOO = "com.example.evan.smartvpn.action.FOO"
private const val ACTION_BAZ = "com.example.evan.smartvpn.action.BAZ"

// TODO: Rename parameters
private const val EXTRA_PARAM1 = "com.example.evan.smartvpn.extra.PARAM1"
private const val EXTRA_PARAM2 = "com.example.evan.smartvpn.extra.PARAM2"

/**
 * An [IntentService] subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
class MyNetWorkService : VpnService() {

    init {

    }

    companion object {
        const val INIT: Int = 0
        const val RUNNING: Int = 1
        const val ERROR: Int = 2


        const val NETADDRESS = "10.0.0.2"
        const val MTU = 1500
    }

    var tag = "MyNetWorkService"

    @Volatile
    private var serviceState: Int = 0

    private var netcon: ParcelFileDescriptor? = null

    private var chsevice :Boolean  = false

    private var netstack : Boolean = false


    override fun onCreate() {
        super.onCreate()

        Chimney.register(ISocket { p0 ->
            var r = this@MyNetWorkService.protect(p0.toInt())
            Log.d("proetect result:" , "++++++++"+r.toString())
            r
        }, IDataFlow { p0, p1 -> CallbackM.instance.updateTraffic(p0, p1) })
    }


    private var serviceBinder = object : IService.Stub() {
        override fun queryServiceState(): Int {

            return this@MyNetWorkService.serviceState
        }

        override fun registerCallback(call: IServiceCallback?): Int {
            Log.d("XXXXXXMMMM", "update how  " + (call == null).toString())
            CallbackM.instance.register(call)
            CallbackM.instance.updateServiceState(this@MyNetWorkService.serviceState, "")
            return 0
        }

        override fun unregisterCallback(call: IServiceCallback?): Int {
            CallbackM.instance.unregister(call)
            return 0
        }

        override fun startService(data: ServiceExtra?): Int {
            Thread {
                this@MyNetWorkService.stopService()
            }.start()
            return 0
        }

    }

    fun updateserviceState(){

        Thread {

            var i = 0
            while (i < 5) {

                Thread.sleep(1000)
                var msg = ""
                if (ERROR == serviceState) {
                    msg = "start vpn failed!!"
                }

                Log.d("XXXXXXMMMM", "update service !!!")
                CallbackM.instance.updateServiceState(this.serviceState, msg)
                i++
            }

        }.start()
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {


        if (intent != null) {
            if (intent.getBooleanExtra("stop_flag", false)) {
                Log.d("service stop", "stop command will execute!")

                Thread{
                    this.stopService()
                }.start()
                return Service.START_NOT_STICKY
            }
        }


        if (RUNNING == serviceState && this.netcon != null) {

           return Service.START_NOT_STICKY
        }

        Thread {
            this.startChimneyService()
        }.start()

        return START_NOT_STICKY
    }


    private  fun startChimneyService()  {

        synchronized(this) {

            var vpn = this.prepareTheData(null)

            if (vpn == null) {
                this.serviceState = ERROR
                Log.e(tag, "load vpn profile failed!!++")
                return
            }

            var server = vpn.getString("server")
            var sport = vpn.getInt("server_port")
            var pass = vpn.getString("password")
            var dns = vpn.getString("dns")

            if (!this.chsevice) {
                chsevice = Chimney.startChimney(server.trim(),
                    sport.toLong(),
                    "127.0.0.1", 1080.toLong(),
                    pass.trim(), CustomApp.instance.filesDir.absolutePath )
            }

            if (this.netcon == null) {

                var builder = Builder().addAddress(NETADDRESS, 0)
                    .addRoute("0.0.0.0", 0)
                    .addDnsServer(dns)
                    .setSession("Chimney")
                    .setMtu(MTU)

                this.netcon = builder.establish()
            }

            if (!this.netstack) {
                Chimney.startNetstackService(this.netcon!!.fd.toLong(), "127.0.0.1:1080", dns)
                this.netstack = true
            }

            if (this.chsevice  && this.netstack
                && this.netcon != null) {
                this.serviceState = RUNNING
            }
            else {
                this.serviceState = ERROR
            }
        }

        this.updateserviceState()
    }


    private  fun stopService(){
        synchronized(this){

            if (this.chsevice) {
                Chimney.stopChimney()
                this.chsevice = false
            }

            if (this.netstack) {
                Chimney.stopNetStackService()
                this.netstack = false
            }

            Thread.sleep(3000)

            if (this.netcon != null) {
                this.netcon!!.close()
                this.netcon = null
            }

            this.serviceState = INIT
        }
        this.updateserviceState()
    }


    private fun clearAll() {
          this.stopService()
    }



    private fun prepareTheData(intent: Intent?): JSONObject {

        var file = File(this.filesDir, "sxxx.json")
        var content = file.readText()
        var json = JSONObject(content)
        return json

    }


    override fun onBind(intent: Intent?): IBinder? {
        Log.d(tag, "onBind")
        return serviceBinder
    }

    override fun onUnbind(intent: Intent?): Boolean {
        //CallbackM.instance.unregister(null)
        Log.d(tag, "onUnbind")
        return super.onUnbind(intent)
    }


    override fun onDestroy() {
        this.clearAll()
        this.serviceState = INIT
        super.onDestroy()
    }





}
